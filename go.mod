module bitbucket.org/tenkoff/easy-aws-elastic-beanstalk-bitbucket-pipelines

require (
	github.com/aws/aws-sdk-go v1.16.15
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190108155000-395948e2f546 // indirect
	golang.org/x/text v0.3.0 // indirect
)
