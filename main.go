package main

import (
	"archive/zip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/aws/aws-sdk-go/service/elasticbeanstalk"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

var versionLabel = time.Now().Format("20060102150405")

func file(path string, info os.FileInfo, err error) error {
	if err != nil {
		log.Print(err)
		return nil
	}

	if info.IsDir() {
		return nil
	}

	files = append(files, path)

	return nil
}

var files []string

func main() {
	start := time.Now()
	defer func() {
		elapsed := time.Since(start)
		log.Printf("Binomial took %s", elapsed)
	}()

	log.SetFlags(log.Lshortfile)
	output := os.Args[1]
	dir := os.Args[2]

	err := filepath.Walk(dir, file)
	if err != nil {
		log.Fatal(err)
	}

	err = zipit(output, files)
	if err != nil {
		log.Fatal(err)
	}

	file, err := os.Open(output)
	if err != nil {
		log.Fatal("Unable to open file %q, %v", err)
	}

	defer file.Close()

	upload_to_s3(output)
	create_new_version()
	deploy_new_version()

}

func zipit(filename string, files []string) error {

	newZipFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	// Add files to zip
	for _, file := range files {
		fmt.Println(file)
		zipfile, err := os.Open(file)
		if err != nil {
			return err
		}
		defer zipfile.Close()

		// Get the file information
		info, err := zipfile.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		// Using FileInfoHeader() above only uses the basename of the file. If we want
		// to preserve the folder structure we can overwrite this with the full path.
		header.Name = file

		// Change to deflate to gain better compression
		// see http://golang.org/pkg/archive/zip/#pkg-constants
		header.Method = zip.Deflate

		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			return err
		}
		if _, err = io.Copy(writer, zipfile); err != nil {
			return err
		}
	}
	return nil
}

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func upload_to_s3(filename string) {
	bucket := os.Getenv("S3_BUCKET")

	file, err := os.Open(filename)
	if err != nil {
		exitErrorf("Unable to open file %q, %v", err)
	}

	defer file.Close()

	// Initialize a session in us-west-2 that the SDK will use to load
	// credentials from the shared credentials file ~/.aws/credentials.
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))},
	)

	// Setup the S3 Upload Manager. Also see the SDK doc for the Upload Manager
	// for more information on configuring part size, and concurrency.
	//
	// http://docs.aws.amazon.com/sdk-for-go/api/service/s3/s3manager/#NewUploader
	uploader := s3manager.NewUploader(sess)

	s3BucketKey := os.Getenv("APPLICATION_NAME") + "/" + versionLabel + "-bitbucket_builds.zip"

	// Upload the file's body to S3 bucket as an object with the key being the
	// same as the filename.
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),

		// Can also use the `filepath` standard library package to modify the
		// filename as need for an S3 object key. Such as turning absolute path
		// to a relative path.
		Key: aws.String(s3BucketKey),

		// The file to be uploaded. io.ReadSeeker is preferred as the Uploader
		// will be able to optimize memory when uploading large content. io.Reader
		// is supported, but will require buffering of the reader's bytes for
		// each part.
		Body: file,
	})
	if err != nil {
		// Print the error and exit.
		exitErrorf("Unable to upload %q to %q, %v", filename, bucket, err)
	}

	fmt.Printf("Successfully uploaded %q to %q\n", filename, bucket)
}

func create_new_version() {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))},
	)
	if err != nil {
		log.Fatalln(err)
	}

	ebc := elasticbeanstalk.New(sess)

	applicationName := os.Getenv("APPLICATION_NAME")
	description := "New build from Bitbucket"

	s3Bucket := os.Getenv("S3_BUCKET")
	s3BucketKey := os.Getenv("APPLICATION_NAME") + "/" + versionLabel + "-bitbucket_builds.zip"

	s3Bundle := elasticbeanstalk.S3Location{
		S3Bucket: &s3Bucket,
		S3Key:    &s3BucketKey,
	}

	a, err := ebc.CreateApplicationVersion(&elasticbeanstalk.CreateApplicationVersionInput{
		ApplicationName: &applicationName,
		VersionLabel:    &versionLabel,
		Description:     &description,
		SourceBundle:    &s3Bundle,
	})
	if err != nil {
		log.Panic(err)
	}
	fmt.Println(a)
}

func deploy_new_version() {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))},
	)
	if err != nil {
		log.Fatalln(err)
	}

	ebc := elasticbeanstalk.New(sess)

	applicationName := os.Getenv("APPLICATION_NAME")
	environmentName := os.Getenv("APPLICATION_ENVIRONMENT")

	e, err := ebc.UpdateEnvironment(&elasticbeanstalk.UpdateEnvironmentInput{
		ApplicationName: &applicationName,
		EnvironmentName: &environmentName,
		VersionLabel:    &versionLabel,
	})
	if err != nil {
		log.Panic(err)
	}
	fmt.Println(e)
}
